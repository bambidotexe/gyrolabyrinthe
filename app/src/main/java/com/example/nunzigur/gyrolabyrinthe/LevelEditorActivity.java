package com.example.nunzigur.gyrolabyrinthe;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import com.example.nunzigur.gyrolabyrinthe.model.Bloc;
import com.example.nunzigur.gyrolabyrinthe.util.EditorView;
import com.example.nunzigur.gyrolabyrinthe.util.LabyrintheEngine;

import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LevelEditorActivity extends Activity {

    // Le moteur graphique du jeu
    private EditorView mView = null;
    // Le moteur physique du jeu
    private LabyrintheEngine mEngine = null;

    private final Object lock = new Object();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_fullscreen);


        mView = new EditorView(this);
        setContentView(mView);


        //mEngine = new LabyrintheEngine(this);



    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int)event.getX();
        int y = (int)event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                pushBloc(x,y);
            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:
        }
        return false;
    }

    private void pushBloc(int x, int y){
        List<Bloc> tmp = mView.getBlocks();
        Boolean isBloc = false;
        int density = (int) mView.getResources().getDisplayMetrics().density;
        int xBloc =x * density / 250;
        int yBloc =y * density / 250;
        for (Bloc bloc : tmp) {
            if (bloc.getX() == xBloc && bloc.getY() == yBloc) {
                isBloc = true;
                switch (bloc.getType().toString()) {
                    case "TROU":
                        mView.removeBlock(xBloc,yBloc);
                        mView.setBlock(new Bloc(Bloc.Type.DEPART, xBloc, yBloc, density));
                        break;
                    case "DEPART":
                        mView.removeBlock(xBloc, yBloc);
                        mView.setBlock(new Bloc(Bloc.Type.ARRIVEE, xBloc, yBloc, density));
                        break;
                    case "ARRIVEE":
                        mView.removeBlock(xBloc,yBloc);
                        break;
                }
                return;
            }
        }
        if (!isBloc)
        {
            mView.setBlock(new Bloc(Bloc.Type.TROU, xBloc, yBloc, density));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //mEngine.resume();
    }

    @Override
    protected void onPause() {
        super.onStop();
        //mEngine.stop();
    }

    @Override
    public void onBackPressed() {
        /*try {
            Context context = this.getApplicationContext();
            FileOutputStream fos = context.openFileOutput("level.xml", Context.MODE_PRIVATE);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null,"level");
            List<Bloc> blocks = new ArrayList<>(mView.getBlocks());
            for (Bloc block: blocks){
                xmlSerializer.startTag(null, "block");
                xmlSerializer.attribute(null, "type", String.valueOf(block.getType()));
                xmlSerializer.attribute(null, "x", String.valueOf(block.getX()));
                xmlSerializer.attribute(null, "y", String.valueOf(block.getY()));
                xmlSerializer.endTag(null, "block");
            }
            xmlSerializer.endTag(null,"level");
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        //Level blocks = new Level(mView.getBlocks());
        //blocks.save();

        super.onBackPressed();
    }


}
