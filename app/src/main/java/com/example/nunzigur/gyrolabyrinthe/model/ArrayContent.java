package com.example.nunzigur.gyrolabyrinthe.model;

/**
 * Created by Rubens-Nzg on 16/03/16.
 */
class ArrayContent {
    private String type;
    private Properties propreties;

    public ArrayContent(String type, Properties propreties) {
        this.type = type;
        this.propreties = propreties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getPropreties() {
        return propreties;
    }

    public void setPropreties(Properties propreties) {
        this.propreties = propreties;
    }
}
