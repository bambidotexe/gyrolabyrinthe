package com.example.nunzigur.gyrolabyrinthe.model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rubens-Nzg on 16/03/16.
 */
public interface MetroInterface {
    @GET("http://data.metromobilite.fr/api/bbox/json?types=arret")
    Call<TotalResults> listRepos(@Path("user") String user);
}
