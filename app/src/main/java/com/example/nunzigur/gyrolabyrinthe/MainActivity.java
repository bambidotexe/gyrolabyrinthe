package com.example.nunzigur.gyrolabyrinthe;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.nunzigur.gyrolabyrinthe.model.Bloc;
import com.example.nunzigur.gyrolabyrinthe.model.TotalResults;
import com.orm.SugarContext;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button playButton = (Button) findViewById(R.id.button_play);
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(MainActivity.this, FullscreenActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        Button editorButton = (Button) findViewById(R.id.button_editor);
        editorButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(MainActivity.this, LevelEditorActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        Button sugarButton = (Button) findViewById(R.id.button_sugar);
        sugarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Context context = getApplicationContext();
                SugarContext.init(context);
                String var = Bloc.findById(Bloc.class, 1).toString();
                Toast toast = Toast.makeText(context, var, Toast.LENGTH_SHORT);
                toast.show();
            }
        });


    }

    private static MetroApiInterface metroApiInterface;

    public static MetroApiInterface getClient(Context ctx){
        if (metroApiInterface == null)
        {
            Retrofit client = new Retrofit.Builder()
                    .baseUrl("http://data.metromobilite.fr/")
                    .build();
            metroApiInterface = client.create(MetroApiInterface.class);
        }
        return metroApiInterface;
    }

    public interface MetroApiInterface {
        @GET("api/bbox/json?types=arret")
        Call<TotalResults> metroList();
    }

}
