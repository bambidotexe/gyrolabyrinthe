package com.example.nunzigur.gyrolabyrinthe.sensors;
        import android.content.Context;
        import android.hardware.Sensor;
        import android.hardware.SensorEvent;
        import android.hardware.SensorEventListener;
        import android.hardware.SensorManager;

public class LightSensor implements SensorEventListener{

    private final SensorManager sensorManager;
    private final Sensor mLight;
    private float result = 0;

    public LightSensor(Context cTx) {
        sensorManager = (SensorManager) cTx.getSystemService(Context.SENSOR_SERVICE);
        mLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    public float getResult() {
        return result;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        result = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void register(){
        sensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unRegister(){
        sensorManager.unregisterListener(this, mLight);
    }
}