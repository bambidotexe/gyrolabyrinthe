package com.example.nunzigur.gyrolabyrinthe.model;

/**
 * Created by Rubens-Nzg on 16/03/16.
 */
public class Properties {
    private String CODE;
    private String LIBELLE;
    private String COMMUNE;

    public Properties(String CODE, String COMMUNE, String LIBELLE) {
        this.CODE = CODE;
        this.COMMUNE = COMMUNE;
        this.LIBELLE = LIBELLE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getCOMMUNE() {
        return COMMUNE;
    }

    public void setCOMMUNE(String COMMUNE) {
        this.COMMUNE = COMMUNE;
    }

    public String getLIBELLE() {
        return LIBELLE;
    }

    public void setLIBELLE(String LIBELLE) {
        this.LIBELLE = LIBELLE;
    }
}
