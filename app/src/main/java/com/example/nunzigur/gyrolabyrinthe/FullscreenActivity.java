package com.example.nunzigur.gyrolabyrinthe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.nunzigur.gyrolabyrinthe.model.Bloc;
import com.example.nunzigur.gyrolabyrinthe.model.Boule;
import com.example.nunzigur.gyrolabyrinthe.util.LabyrintheEngine;
import com.example.nunzigur.gyrolabyrinthe.util.LabyrintheView;

import java.io.IOException;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends Activity {
    // Identifiant de la boîte de dialogue de victoire
    public static final int VICTORY_DIALOG = 0;
    // Identifiant de la boîte de dialogue de défaite
    public static final int DEFEAT_DIALOG = 1;

    // Le moteur graphique du jeu
    private LabyrintheView mView = null;
    // Le moteur physique du jeu
    private LabyrintheEngine mEngine = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_fullscreen);


        mView = new LabyrintheView(this);
        setContentView(mView);

        mEngine = new LabyrintheEngine(this);

        Boule b = new Boule();
        mView.setBoule(b);
        mEngine.setBoule(b);

        List<Bloc> mList = null;
        try {
            int level = 1;
            mList = mEngine.buildLabyrinthe(level);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mView.setBlocks(mList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEngine.resume();
    }

    @Override
    protected void onPause() {
        super.onStop();
        mEngine.stop();
    }

    @Override
    public Dialog onCreateDialog (int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch(id) {
        case VICTORY_DIALOG:
            builder.setCancelable(false)
            .setMessage("Bravo, vous avez gagné !")
            .setTitle("Champion ! Le roi des Zörglubienotchs est mort grâce à vous !")
            .setNeutralButton("Menu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // L'utilisateur peut recommencer s'il le veut
                    Intent myIntent = new Intent(FullscreenActivity.this, MainActivity.class);
                    FullscreenActivity.this.startActivity(myIntent);
                }
            });
            break;

        case DEFEAT_DIALOG:
            builder.setCancelable(false)
            .setMessage("La Terre a été détruite à cause de vos erreurs.")
            .setTitle("Bah bravo !")
            .setNeutralButton("Menu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent(FullscreenActivity.this, MainActivity.class);
                    FullscreenActivity.this.startActivity(myIntent);
                }
            });
        }
        return builder.create();
    }

    @Override
    public void onPrepareDialog (int id, Dialog box) {
        // A chaque fois qu'une boîte de dialogue est lancée, on arrête le moteur physique
        mEngine.stop();
    }



}
