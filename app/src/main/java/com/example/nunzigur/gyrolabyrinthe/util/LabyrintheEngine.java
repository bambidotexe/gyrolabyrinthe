package com.example.nunzigur.gyrolabyrinthe.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Service;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Xml;

import com.example.nunzigur.gyrolabyrinthe.FullscreenActivity;
import com.example.nunzigur.gyrolabyrinthe.model.Bloc;
import com.example.nunzigur.gyrolabyrinthe.model.Boule;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class LabyrintheEngine {
    private Boule mBoule = null;
    public Boule getBoule() {
        return mBoule;
    }

    public void setBoule(Boule pBoule) {
        this.mBoule = pBoule;
    }

    // Le labyrinthe
    private List<Bloc> mBlocks = null;

    private FullscreenActivity mActivity = null;

    private SensorManager mManager = null;
    private Sensor mAccelerometre = null;
    private boolean firstLaunch = false;
    private float xLaunch, yLaunch;

    private final SensorEventListener mSensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent pEvent) {

            if(firstLaunch==false)
            {
                xLaunch = pEvent.values[0];
                yLaunch = pEvent.values[1];
                firstLaunch = true;
            }
            float x = pEvent.values[0]-xLaunch;
            float y = pEvent.values[1]-yLaunch;

            if(mBoule != null) {
                // On met à jour les coordonnées de la boule
                RectF hitBox = mBoule.putXAndY(x, y);

                // Pour tous les blocs du labyrinthe
                for(Bloc block : mBlocks) {
                    // On crée un nouveau rectangle pour ne pas modifier celui du bloc
                    RectF inter = new RectF(block.getRectangle());
                    if(inter.intersect(hitBox)) {
                        // On agit différement en fonction du type de bloc
                        switch(block.getType()) {
                            case TROU:
                                float mX = x;

                                // Si la boule sort du cadre, on rebondit
                                /*if(mX < mBoule.RAYON) {
                                    mX = mBoule.RAYON;
                                    // Rebondir, c'est changer la direction de la balle
                                    mSpeedY = -mSpeedY / mBoule.REBOND;
                                } else if(mX > block.x - mBoule.RAYON) {
                                    mX = mWidth - mBoule.RAYON;
                                    mSpeedY = -mSpeedY / mBoule.REBOND;
                                }*/
                                mActivity.showDialog(FullscreenActivity.DEFEAT_DIALOG);
                                break;

                            case DEPART:
                                break;

                            case ARRIVEE:
                                mActivity.showDialog(FullscreenActivity.VICTORY_DIALOG);
                                break;
                        }
                        break;
                    }
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor pSensor, int pAccuracy) {

        }
    };

    public LabyrintheEngine(FullscreenActivity pView) {
        mActivity = pView;
        mManager = (SensorManager) mActivity.getBaseContext().getSystemService(Service.SENSOR_SERVICE);
        mAccelerometre = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    // Remet à zéro l'emplacement de la boule
    public void reset() {
        firstLaunch = false;
        mBoule.reset();
    }

    // Arrête le capteur
    public void stop() {
        mManager.unregisterListener(mSensorEventListener, mAccelerometre);
    }

    // Redémarre le capteur
    public void resume() {
        mManager.registerListener(mSensorEventListener, mAccelerometre, SensorManager.SENSOR_STATUS_ACCURACY_LOW);
    }

    // Construit le labyrinthe
    public List<Bloc> buildLabyrinthe(int i) throws IOException {
        try {
            InputStream input = mActivity.getApplicationContext().getAssets().open("level1.xml");
            //FileInputStream input = mActivity.getApplicationContext().openFileInput("level.xml");

            mBlocks= parse(input);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        return mBlocks;

    }

    private static final String ns = null;


    private List<Bloc> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readLevel(parser);
        } finally {
            in.close();
        }
    }


    // Parses the contents of an level. If it encounters a title, summary, or link tag, hands them off
    // to their respective "read" methods for processing. Otherwise, skips the tag.
    private List<Bloc> readLevel(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "level");
        List<Bloc> blocks = new ArrayList<>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("block")) {
                blocks.add(readBloc(parser));
                parser.next();
            }else {
                skip(parser);
            }
        }
        return blocks;
    }

    // Processes bloc tags in the level.
    private Bloc readBloc(XmlPullParser parser) throws IOException, XmlPullParserException {
        float density = mActivity.getResources().getDisplayMetrics().density;
        Bloc bloc = null;
        parser.require(XmlPullParser.START_TAG, ns, "block");
        String i = parser.getAttributeValue(null, "x");
        String j = parser.getAttributeValue(null, "y");
        String type = parser.getAttributeValue(null, "type");
        if(type.equalsIgnoreCase("TROU")){
            bloc = new Bloc(Bloc.Type.TROU, Integer.valueOf(i), Integer.parseInt(j), density);
        }
        if(type.equalsIgnoreCase("DEPART")){
            bloc = new Bloc(Bloc.Type.DEPART, Integer.valueOf(i), Integer.parseInt(j), density);
            mBoule.setInitialRectangle(new RectF(bloc.getRectangle()));

        }
        if(type.equalsIgnoreCase("ARRIVEE")) {
            bloc = new Bloc(Bloc.Type.ARRIVEE, Integer.valueOf(i), Integer.parseInt(j), density);
            bloc.save();
        }
        //parser.require(XmlPullParser.END_TAG, ns, "block");
        return bloc;
    }

    // For the tags level extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


}
