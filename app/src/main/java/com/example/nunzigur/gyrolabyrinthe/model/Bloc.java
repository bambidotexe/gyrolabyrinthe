package com.example.nunzigur.gyrolabyrinthe.model;

import android.graphics.RectF;

import com.example.nunzigur.gyrolabyrinthe.model.Boule;
import com.orm.SugarRecord;

public class Bloc extends SugarRecord {
    public enum  Type { TROU, DEPART, ARRIVEE };
    private final float SIZE = Boule.RAYON * 1.3f;
    private int x;
    private int y;
    private Type mType = null;
    private RectF mRectangle = null;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Type getType() {
        return mType;
    }

    public RectF getRectangle() {
        return mRectangle;
    }

    public Bloc(Type pType, int pX, int pY, float density) {
        this.x = pX;
        this.y = pY;
        this.mType = pType;
        this.mRectangle = new RectF(pX * SIZE*density, pY * SIZE*density, (pX + 1) * SIZE*density, (pY + 1) * SIZE*density);
    }
}
