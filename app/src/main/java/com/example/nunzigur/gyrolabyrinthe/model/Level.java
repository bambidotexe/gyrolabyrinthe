package com.example.nunzigur.gyrolabyrinthe.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Rubens-Nzg on 16/03/16.
 */
class Level extends SugarRecord {
    private List<Bloc> blocks;

    public Level(List<Bloc> blocks) {
        this.blocks = blocks;
    }

    public List<Bloc> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Bloc> blocks) {
        this.blocks = blocks;
    }
}
