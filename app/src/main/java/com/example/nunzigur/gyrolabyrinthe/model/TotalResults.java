package com.example.nunzigur.gyrolabyrinthe.model;

import java.util.List;

/**
 * Created by Rubens-Nzg on 16/03/16.
 */
public class TotalResults {
    private String type;
    private List<ArrayContent> features;

    public TotalResults(String type, List<ArrayContent> features) {
        this.type = type;
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ArrayContent> getFeatures() {
        return features;
    }

    public void setFeatures(List<ArrayContent> features) {
        this.features = features;
    }
}
