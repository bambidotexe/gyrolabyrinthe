package com.example.nunzigur.gyrolabyrinthe.util;

import com.example.nunzigur.gyrolabyrinthe.model.Bloc;

import java.util.List;

/**
 * Created by Rubens-Nzg on 02/03/16.
 */
class Level {
    private final String name;
    private final List<Bloc> levelBlocks;

    public Level(String name, List<Bloc> levelBlocks) {
        this.name = name;
        this.levelBlocks = levelBlocks;
    }


}





